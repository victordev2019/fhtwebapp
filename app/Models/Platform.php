<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    // RELACIONES BASE DE DATOS
    // relación uno a múchos
    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }
}
