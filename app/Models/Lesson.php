<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    // RELACIONES BASE DE DATOS
    // relación uno a uno inversa
    public function description()
    {
        return $this->hasOne(Description::class);
    }
    // relación uno a múchos inversa
    public function section()
    {
        return $this->belongsTo(Section::class);
    }
    public function platform()
    {
        return $this->belongsTo(Platform::class);
    }
    // relación múchos a múchos inversa
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
    // RELACIONES POLIMORFICAS
    // relacion uno a uno poli
    public function resource()
    {
        return $this->morphOne(Resource::class, 'resourceable');
    }
    // relacion uno a muchos poli
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }
}
