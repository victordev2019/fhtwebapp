<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    // RELACIONES BASE DE DATOS
    // relación uno a múchos
    public function lesson()
    {
        return $this->hasMany(Lesson::class);
    }
    // relación uno a múchos inversa
    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
