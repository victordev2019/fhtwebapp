<?php

namespace Database\Seeders;

use App\Models\Platform;
use Illuminate\Database\Seeder;
use Livewire\Features\Placeholder;

class PlatformSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Platform::create([
            'name' => 'Youtube'
        ]);
        Platform::create([
            'name' => 'Vimeo'
        ]);
    }
}
